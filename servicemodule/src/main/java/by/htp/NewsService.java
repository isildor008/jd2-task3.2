package by.htp;

import by.htp.dao.NewsDao;
import by.htp.entity.News;
import by.htp.entity.NewsBuilder;
import by.htp.serviceException.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Collections;
import java.util.List;

import static by.htp.serviceException.ConstantService.*;

/**
 * Created by Dmitry on 26.09.2017.
 */
@Transactional
@Service("newsService")
public class NewsService {
    private static Logger log = Logger.getLogger(NewsService.class.getName());
    @Autowired
    private NewsDao newsDao;

    public List<News> getAllNews() {
        List<News> newsList = newsDao.getAllNews();
        Collections.sort(newsList);
                return newsList;

    }

    public News getNewsById(int id) throws ServiceException {
        if (0 != id) {
            News news = newsDao.getById(id);
            return news;
        } else {
            log.error(EXRPTIONGETBYID);
            throw new ServiceException(EXRPTIONGETBYID);
        }
    }


    public void delete(int id) throws ServiceException {
        if (0 != id) {
            News news = newsDao.getById(id);
            news.setStatus(false);
            newsDao.update(news);
        } else {
            log.error(EXCEPTIONDELETEMESSAGE);
            throw new ServiceException(EXCEPTIONDELETEMESSAGE);
        }
    }

    public void add(NewsBuilder newsBuilder) throws ServiceException {
        if (null != newsBuilder) {
            newsBuilder.setStatus(true);
            newsDao.save(newsBuilder.build());
        } else {
            log.error(EXCEPTIONADDMESSAGE);
            throw new ServiceException(EXCEPTIONADDMESSAGE);
        }

    }

    public void update(News news) throws ServiceException {
        if (null != news) {
            newsDao.update(news);
        } else {
            log.error(EXEPTIONUPDATE);
            throw new SecurityException(EXEPTIONUPDATE);
        }

    }
    public void restore(int id) throws ServiceException {
        if (0 != id) {
            News news = newsDao.getById(id);
            news.setStatus(true);
            newsDao.update(news);
        } else {
            log.error(EXCEPTIONDELETEMESSAGE);
            throw new ServiceException(EXCEPTIONDELETEMESSAGE);
        }
    }
}
