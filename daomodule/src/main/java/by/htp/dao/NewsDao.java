package by.htp.dao;

import by.htp.entity.News;

import java.util.List;

/**
 * Created by Dmitry on 26.09.2017.
 */
public interface NewsDao extends BaseDao {
    News getById(int id);
    List<News> getAllNews();
}
