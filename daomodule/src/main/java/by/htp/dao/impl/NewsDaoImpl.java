package by.htp.dao.impl;

import by.htp.entity.News;
import by.htp.dao.NewsDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dmitry on 26.09.2017.
 */
@Repository
public class NewsDaoImpl extends BaseDaoImpl implements NewsDao {
    private static final SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    public News getById(int id) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            News news=session.get(News.class,id);
            session.getTransaction().commit();
            return news;
        } finally {
            session.close();
        }

    }
    public List<News> getAllNews() {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            List list = session.createCriteria(News.class).list();
            session.getTransaction().commit();
            return list;
        } finally {
            session.close();
        }
    }
}

