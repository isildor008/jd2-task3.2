package by.htp.dao;

/**
 * Created by Dmitry on 26.09.2017.
 */
public interface BaseDao {

    void save(Object o);

    void update(Object o);

    void delete(Object o);
}
