package by.htp.controller;

/**
 * Created by Dmitry on 29.09.2017.
 */
public class ConstantController {
    public static final String NEWSSAVED="News is saved in Data Base.";
    public static final String ERRORSAVEDNEWS="Error during saving news in Data Base.";
    public static final String REMOVENEWS="News is removed";
    public static final String ERRORREMOVENEWS="Error during saving news in Data Base";
    public static final String UPDATENEWS="News is updated";
    public static final String ERRORUPDATENEWS="Error during updating news in Data Base";
    public static final String RESTORENEWS="News is restored";
}
