package by.htp.controller;


import by.htp.NewsService;
import by.htp.entity.DeleteNews;
import by.htp.entity.NewsBuilder;
import by.htp.serviceException.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static by.htp.controller.ConstantController.*;


/**
 * Created by Dmitry on 26.09.2017.
 */
@Controller
public class NewsController {

    @Autowired
    NewsService newsService;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @RequestMapping(value = "/newsList", method = RequestMethod.GET)
    public String newsList(Model model) {
        List<DeleteNews> deleteSomeNews = new ArrayList<DeleteNews>();
        model.addAttribute("newsList", newsService.getAllNews());
        model.addAttribute("deleteSomeNews", deleteSomeNews);
        return "newsListPage";
    }

    @RequestMapping(value = "/deletedNewsPage", method = RequestMethod.GET)
    public String newsListDeleted(Model model) {
        model.addAttribute("newsList", newsService.getAllNews());
        return "deletedNewsPage";
    }

    @RequestMapping(value = "/deleteSomeNews", method = RequestMethod.POST)
    public String deleteNews(@ModelAttribute("deleteSomeNews") DeleteNews deleteNews) {
        List<DeleteNews> list = new ArrayList<DeleteNews>();
        list.add(deleteNews);
        System.out.println(list);

        return "redirect:/viewNews?id=";
    }


    @RequestMapping(value = "/editNews", method = RequestMethod.GET)
    public String editNewsGet(Model model, @RequestParam int id) {
        try {
            model.addAttribute("news", newsService.getNewsById(id));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return "editNewsPage";
    }

    @RequestMapping(value = "/editNews", method = RequestMethod.POST)
    public String editNewsPost(@ModelAttribute("news") NewsBuilder newsBuilder, @RequestParam int id, RedirectAttributes redirectAttributes) {
        try {
            newsService.update(newsBuilder.build());
            redirectAttributes.addFlashAttribute("message", UPDATENEWS);
        } catch (ServiceException e) {
            redirectAttributes.addFlashAttribute("message", ERRORUPDATENEWS);
        }
        return "redirect:/viewNews?id=" + id;
    }

    @RequestMapping(value = "/viewNews", method = RequestMethod.GET)
    public String viewNewsGet(Model model, @RequestParam int id) {
        try {
            model.addAttribute("news", newsService.getNewsById(id));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return "viewNewsPage";
    }

    @RequestMapping(value = "/viewNewsDeleted", method = RequestMethod.GET)
    public String viewNewsGetDeleted(Model model, @RequestParam int id) {
        try {
            model.addAttribute("news", newsService.getNewsById(id));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return "viewDeletedNewsPage";
    }

    @RequestMapping(value = "/deleteNews", method = RequestMethod.GET)
    public String deleteNews(@RequestParam int id, RedirectAttributes redirectAttributes) {
        try {
            newsService.delete(id);
            redirectAttributes.addFlashAttribute("message", REMOVENEWS);
        } catch (ServiceException e) {
            redirectAttributes.addFlashAttribute("message", ERRORREMOVENEWS);
            return "redirect:/newsList";
        }
        return "redirect:/newsList";
    }

    @RequestMapping(value = "/restoreNews", method = RequestMethod.GET)
    public String restoreNews(@RequestParam int id, RedirectAttributes redirectAttributes) {
        try {
            newsService.restore(id);
            redirectAttributes.addFlashAttribute("message", RESTORENEWS);
        } catch (ServiceException e) {
            redirectAttributes.addFlashAttribute("message", ERRORREMOVENEWS);
            return "redirect:/newsList";
        }
        return "redirect:/newsList";
    }

    @RequestMapping(value = "/addNews", method = RequestMethod.GET)
    public String addNewsGet(Model model) {
        model.addAttribute("news", new NewsBuilder());
        return "addNewsPage";
    }

    @RequestMapping(value = "/addNews", method = RequestMethod.POST)
    public String addNewsPost(@Valid @ModelAttribute("news") NewsBuilder newsBuilder, BindingResult theBindingResult, RedirectAttributes redirectAttributes) {
        if (theBindingResult.hasErrors()) {
            return "/addNewsPage";
        } else {
            try {
                newsService.add(newsBuilder);
                redirectAttributes.addFlashAttribute("message", NEWSSAVED);
            } catch (ServiceException e) {
                redirectAttributes.addFlashAttribute("message", ERRORSAVEDNEWS);
                return "redirect:/newsList";
            }
            return "redirect:/newsList";
        }
    }


}
