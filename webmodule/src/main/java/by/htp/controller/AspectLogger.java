package by.htp.controller;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;


/**
 * Created by Dmitry on 27.09.2017.
 */
@Aspect
public class AspectLogger {
    private static Logger log = Logger.getLogger(AspectLogger.class.getName());
    @Pointcut("within(by.htp.controller.*)")
    public void exec(){}

    @Before("exec()")
    public void beforeControllers(JoinPoint joinPoint) {
        log.info(joinPoint.getSignature().getName());

    }
}
