package by.htp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Dmitry on 26.09.2017.
 */
@Controller

public class RedirectController {

    @RequestMapping("/")
    public String redirectMethod(){
        return "redirect:/newsList";
    }

}
