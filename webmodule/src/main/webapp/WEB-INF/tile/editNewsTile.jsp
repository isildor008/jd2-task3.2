<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 29.09.2017
  Time: 12:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form:form action="editNews" modelAttribute="news" method="post">
    <form:hidden path="id"/>
    <form:hidden path="status"/>
    <section class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="/newsList"><spring:message code="newsList"/></a></li>
                        <li><a href="/addNews"><spring:message code="addNews"/></a></li>
                        <li><a href="/deletedNewsPage"><spring:message code="deletedNews"/></a></li>
                    </ul>
                </div>
                <div class="col-xs-9 col-xs-offset-3 col-md-10 col-md-offset-2 main-content">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ol class="breadcrumb">
                                <li class="active"><a href="/newsList"><spring:message code="news"/></a></li>
                                <li class="active"><spring:message code="editNews"/></li>
                            </ol>
                        </div>
                        <div class="panel-body">
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="newsTitle"/></label></div>
                                <div class="col-md-10">
                                        <%--<input class="form-control"></input>--%>
                                    <form:input id="title" class="form-control" type="text" name="title"
                                                placeholder="title"
                                                path="title"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="newsDate"/></label></div>
                                <div class="col-md-10">
                                        <%--<input class="form-control"></input>--%>
                                    <form:input id="date" class="form-control" type="date" name="date" placeholder="date"
                                                path="date"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="brief"/></label></div>
                                <div class="col-md-10">
                                        <%--<textarea class="form-control"></textarea>--%>
                                    <form:textarea id="brief" class="form-control" name="brief" placeholder="brief"
                                                   path="brief"/>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="content"/></label></div>
                                <div class="col-md-10">
                                        <%--<textarea class="form-control"></textarea>--%>
                                    <form:textarea id="content" class="form-control" name="content"
                                                   placeholder="content"
                                                   path="content"/>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-primary"><spring:message code="save"/></button>
                                <%--<button type="button" class="btn btn-default"><spring:message code="cancel"/></button>--%>
                                <a href="/viewNews?id=${news.getId()}" class="btn btn-default"><spring:message code="cancel"/></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form:form>
