<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 29.09.2017
  Time: 12:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class="active"><a href="/newsList"><spring:message code="newsList"/></a></li>
                    <li><a href="/addNews"><spring:message code="addNews"/></a></li>
                    <li><a href="/deletedNewsPage"><spring:message code="deletedNews"/></a></li>
                </ul>
            </div>
            <div class="col-xs-9 col-xs-offset-3 col-md-10 col-md-offset-2 main-content">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <ol class="breadcrumb">
                            <li><a href="/newsList"><spring:message code="news"/></a></li>
                            <li class="active"><spring:message code="deletedNews"/></li>
                        </ol>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <c:forEach items="${newsList}" var="newsList">

                                <h2 class="news-title">
                                <c:if test="${newsList.getStatus()==false}">
                                    <c:out value="${newsList.getTitle()}"/> <span>
               <fmt:formatDate pattern="yyyy/MM/dd" value="${newsList.getDate()}"/></span>
                                    </h2>
                                    <div class="news-content"><c:out value="${newsList.getBrief()}"/></div>
                                    <div class="news-control pull-right">
                                        <a href="/viewNewsDeleted?id=${newsList.getId()}"
                                           class="btn btn-primary"><spring:message code="view"/></a>
                                        <a href="/editNews?id=${newsList.getId()}"
                                           class="btn btn-primary btn-success"><spring:message code="edit"/></a>
                                        <a href="/restoreNews?id=${newsList.getId()}" class="btn btn-danger pull-right"><spring:message code="restoreNews"/></a>


                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>

                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>