<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 29.09.2017
  Time: 12:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form:form action="viewNews" id="id" modelAttribute="news">
    <section class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="/newsList"><spring:message code="newsList"/></a></li>
                        <li><a href="/addNews"><spring:message code="addNews"/></a></li>
                        <li><a href="/deletedNewsPage"><spring:message code="deletedNews"/></a></li>
                    </ul>
                </div>
                <div class="col-xs-9 col-xs-offset-3 col-md-10 col-md-offset-2 main-content">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ol class="breadcrumb">
                                <li class="active"><a href="/newsList"><spring:message code="newsList"/></a></li>
                                <li class="active"><spring:message code="view"/></li>
                            </ol>
                        </div>
                        <div class="panel-body">
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="newsTitle"/></label></div>
                                <div class="col-md-10">
                                    <p><c:out value="${news.getTitle()}"/></p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="newsDate"/></label></div>
                                <div class="col-md-10">
                                    <p><c:out value="${news.getDate()}"/></p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="brief"/></label></div>
                                <div class="col-md-10">
                                    <p><c:out value="${news.getBrief()}"/></p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2"><label><spring:message code="content"/></label></div>
                                <div class="col-md-10">
                                    <p><c:out value="${news.getContent()}"/></p>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <a href="/editNews?id=${news.getId()}" class="btn btn-primary"><spring:message code="edit"/></a>
                                <a href="/deleteNews?id=${news.getId()}" class="btn btn-danger"><spring:message code="delete"/></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form:form>