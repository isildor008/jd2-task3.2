<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 29.09.2017
  Time: 12:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="main">
    <form:form action="addNews" id="id" modelAttribute="news" method="post">
    <form:hidden path="id"/>
    <form:hidden path="status"/>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class="active"><a href="/newsList"><spring:message code="newsList"/></a></li>
                    <li><a href="/addNews"><spring:message code="addNews"/></a></li>
                    <li><a href="/deletedNewsPage"><spring:message code="deletedNews"/></a></li>
                </ul>
            </div>
            <div class="col-xs-9 col-xs-offset-3 col-md-10 col-md-offset-2 main-content">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <ol class="breadcrumb">
                            <li class="active"><a href="/newsList"><spring:message code="news"/></a></li>
                            <li class="active"><spring:message code="addNews"/></li>
                        </ol>
                    </div>
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-xs-2"><label><spring:message code="newsTitle"/><span style="color: red">*</span></label></div>
                            <div class="col-md-10">
                                <form:input id="title" class="form-control" type="text"  name="title" placeholder="title"
                                               path="title"/>
                                <form:errors path="title" cssClass="error"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-2"><label><spring:message code="newsDate"/><span style="color: red">*</span></label></div>
                            <div class="col-md-10">
                                <form:input id="date" class="form-control" type="date" name="date" placeholder="date"
                                            path="date"/>
                                <form:errors path="date" cssClass="error"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-2"><label><spring:message code="brief"/><span style="color: red">*</span></label></div>
                            <div class="col-md-10">
                                <form:textarea id="brief" type="text" name="brief" placeholder="brief"
                                               path="brief"/>
                                <form:errors path="brief" cssClass="error"/>

                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-2"><label><spring:message code="content"/><span style="color: red">*</span></label></div>
                            <div class="col-md-10">
                                <form:textarea id="content" type="text" name="content" placeholder="content"
                                               path="content"/>
                                <form:errors path="content" cssClass="error"/>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <button type="submit" class="btn btn-primary"><spring:message code="save"/></button>
                            <a href="/newsList" class="btn btn-primary "><spring:message code="cancel"/></a>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>