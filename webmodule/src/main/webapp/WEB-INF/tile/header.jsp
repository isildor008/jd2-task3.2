<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 29.09.2017
  Time: 11:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <a class="navbar-brand" href="/newsList"><spring:message code="newsManagement"/> </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <%--<li class="message" style="color: indianred"><c:out value="${message}"/> </li>--%>
                <%--<div class="btn btn-danger pull-right"> <c:out value="${message}"/></div>--%>
                <li><a style="color: palevioletred"> <c:out value="${message}"/> </a></li>
                <li><a href="?languageVar=en_EN"> <spring:message code="english"/> </a></li>
                <li><a href="?languageVar=ru_RU"> <spring:message code="russian"/></a></li>
            </ul>
        </div>
    </div>
</nav>