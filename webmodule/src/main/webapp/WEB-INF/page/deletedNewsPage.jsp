<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 29.09.2017
  Time: 22:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>News</title>
    <%@include file="/WEB-INF/static/common_css.jsp" %>
    <%@include file="/WEB-INF/static/common_js.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/tile/header.jsp" %>

<%@include file="/WEB-INF/tile/deletedNewsListTile.jsp" %>

<%@include file="/WEB-INF/tile/footer.jsp" %>
</body>
</html>