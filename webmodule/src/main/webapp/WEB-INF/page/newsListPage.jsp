<%@ page contentType="text/html;charset=UTF-8" language="java" %>



<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>News</title>
    <%@include file="/WEB-INF/static/common_css.jsp" %>
    <%@include file="/WEB-INF/static/common_js.jsp" %>
</head>
<body>
<%@include file="/WEB-INF/tile/header.jsp" %>

<%@include file="/WEB-INF/tile/newsListTile.jsp" %>

<%@include file="/WEB-INF/tile/footer.jsp" %>
</body>
</html>