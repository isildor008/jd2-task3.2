package by.htp.entity;

import org.springframework.stereotype.Repository;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Dmitry on 27.09.2017.
 */
@Repository
public class NewsBuilder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "is required")
    @Size(min = 1, max = 100, message = "is required")
    private String title;

    @NotNull(message = "is required")
    private Date date;

    @NotNull(message = "is required")
    @Size(min = 1, max = 500, message = "max 500 charts")
    private String brief;

    @NotNull(message = "is required")
    @Size(min = 1, max = 2048, message = "is required")
    private String content;

    private boolean status;

    public NewsBuilder id(int id){
        this.id=id;
        return this;
    }

    public NewsBuilder title(String title){
        this.title=title;
        return this;
    }

    public NewsBuilder date(Date date){
        this.date=date;
        return this;
    }

    public NewsBuilder brief(String brief){
        this.brief=brief;
        return this;
    }

    public NewsBuilder content(String content){
        this.content=content;
        return this;    }



    public NewsBuilder status(boolean status){
        this.status=status;
        return this;
    }


   public News build(){
       News news= new News();
       news.setId(id);
       news.setDate(date);
       news.setTitle(title);
       news.setBrief(brief);
       news.setContent(content);
       news.setStatus(status);
       return news;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsBuilder that = (NewsBuilder) o;

        if (id != that.id) return false;
        if (status != that.status) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (brief != null ? !brief.equals(that.brief) : that.brief != null) return false;
        return content != null ? content.equals(that.content) : that.content == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (brief != null ? brief.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (status ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsBuilder{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", date=" + date +
                ", brief='" + brief + '\'' +
                ", content='" + content + '\'' +
                ", status=" + status +
                '}';
    }
}
