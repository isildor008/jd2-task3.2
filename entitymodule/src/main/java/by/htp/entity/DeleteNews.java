package by.htp.entity;

import org.springframework.stereotype.Component;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Created by Dmitry on 29.09.2017.
 */
@Component
public class DeleteNews {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String newsId;

    public DeleteNews(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeleteNews that = (DeleteNews) o;

        if (id != that.id) return false;
        return newsId != null ? newsId.equals(that.newsId) : that.newsId == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DeleteNews{" +
                "id=" + id +
                ", newsId='" + newsId + '\'' +
                '}';
    }
}
